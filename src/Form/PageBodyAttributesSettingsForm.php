<?php

namespace Drupal\page_body_attributes\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * {@inheritdoc}
 */
class PageBodyAttributesSettingsForm extends ConfigFormBase {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->configFactory = $container->get('config.factory');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'pagecss_class_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'page_body_attributes.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $config = $this->config('page_body_attributes.settings');
    $form['desc'] = [
      '#type' => 'markup',
      '#markup' => $this->t('
        <b>Enabled:</b> Rule will work only if checkbox is checked.<br>
        <b>Pages:</b> Enter one path per line. The "*" character is a wildcard. Example paths are "/node/1" for an individual piece of content or "/node/*" for every piece of content. "@front" is the front page.<br>'),
    ];
    // Headers for table.
    $header = [
      $this->t('Enabled'),
      $this->t('Pages'),
      $this->t('Class'),
      $this->t('ID'),
      $this->t('Operation'),
      $this->t('Weight'),
    ];
    // Table form.
    $form['page_attributes_table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#empty' => $this->t('There are no items yet. Add an item.', []),
      '#prefix' => '<div id="pagecss-fieldset-wrapper">',
      '#suffix' => '</div>',
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'page_attributes_table-order-weight',
        ],
      ],
    ];
    // Set table values on Add/Remove or on page load.
    $pagecss_table = $form_state->get('page_attributes_table');
    if (empty($pagecss_table)) {
      // Set data from configuration on page load.
      // Set empty element if no configurations are set.
      if (NULL !== $config->get('page_attributes_table')) {
        $pagecss_table = $config->get('page_attributes_table');
        $form_state->set('page_attributes_table', $pagecss_table);
      }
      else {
        $pagecss_table = [''];
        $form_state->set('page_attributes_table', $pagecss_table);
      }
    }
    // Create row for table.
    foreach ($pagecss_table as $i => $value) {
      $form['page_attributes_table'][$i]['#attributes']['class'][] = 'draggable';
      $form['page_attributes_table'][$i]['status'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Status'),
        '#title_display' => 'invisible',
        '#default_value' => $value['status'] ?? '',
      ];
      $form['page_attributes_table'][$i]['pages'] = [
        '#type' => 'textarea',
        '#title' => $this->t('Pages'),
        '#title_display' => 'invisible',
        '#required' => TRUE,
        '#cols' => '5',
        '#rows' => '5',
        '#default_value' => $value['pages'] ?? [],
      ];
      $form['page_attributes_table'][$i]['class_body'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Class'),
        '#title_display' => 'invisible',
        '#required' => TRUE,
        '#size' => 10,
        '#default_value' => $value['class_body'] ?? [],
      ];
      $form['page_attributes_table'][$i]['id_body'] = [
        '#type' => 'textfield',
        '#title' => $this->t('ID'),
        '#title_display' => 'invisible',
        '#size' => 10,
        '#default_value' => $value['id_body'] ?? [],
      ];
      $form['page_attributes_table'][$i]['remove'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove'),
        '#name' => "remove-" . $i,
        '#submit' => ['::removeElement'],
        '#limit_validation_errors' => [],
        '#ajax' => [
          'callback' => '::removeCallback',
          'wrapper' => 'pagecss-fieldset-wrapper',
        ],
        '#index_position' => $i,
      ];
      // TableDrag: Weight column element.
      $form['page_attributes_table'][$i]['weight'] = [
        '#type' => 'weight',
        '#title_display' => 'invisible',
        '#default_value' => $value['weight'] ?? [],
        '#attributes' => ['class' => ['page_attributes_table-order-weight']],
      ];
    }

    $form['add_name'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add one more'),
      '#submit' => ['::addOne'],
      '#ajax' => [
        'callback' => '::addmoreCallback',
        'wrapper' => 'pagecss-fieldset-wrapper',
      ],
    ];

    $form_state->setCached(FALSE);
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function addmoreCallback(array &$form, FormStateInterface $form_state) {
    return $form['page_attributes_table'];
  }

  /**
   * {@inheritdoc}
   */
  public function addOne(array &$form, FormStateInterface $form_state) {
    $pagecss_table = $form_state->get('page_attributes_table');
    array_push($pagecss_table, "");
    $form_state->set('page_attributes_table', $pagecss_table);
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function removeCallback(array &$form, FormStateInterface $form_state) {
    return $form['page_attributes_table'];
  }

  /**
   * {@inheritdoc}
   */
  public function removeElement(array &$form, FormStateInterface $form_state) {
    // Get table.
    $pagecss_table = $form_state->get('page_attributes_table');
    // Get element to remove.
    $remove = key($form_state->getValue('page_attributes_table'));
    // Remove element.
    unset($pagecss_table[$remove]);
    // Set an empty element if no elements are left.
    if (empty($pagecss_table)) {
      array_push($pagecss_table, "");
    }
    $form_state->set('page_attributes_table', $pagecss_table);
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('page_body_attributes.settings')->set('page_attributes_table', $form_state->getValue('page_attributes_table'))->save();
    parent::submitForm($form, $form_state);
  }

}
